/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arg_no_leading_zero.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jebae <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 16:12:41 by jebae             #+#    #+#             */
/*   Updated: 2019/10/24 16:12:42 by jebae            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t			fp_arg_no_leading_zero(t_fp_tags *tags, size_t length)
{
	(void)tags;
	(void)length;
	return (0);
}
